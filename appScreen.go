package main

import (
	"fmt"
	"github.com/gdamore/tcell"
	"github.com/gdamore/tcell/encoding"
	"os"
)

// appScreen describes the application screen surface
type appScreen struct {
	objects []GUIChild
	tcell.Screen
}

func initScreen() tcell.Screen {
	encoding.Register()
	tcell.SetEncodingFallback(tcell.EncodingFallbackASCII)
	scr, e := tcell.NewScreen()
	if e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%v\n", e)
		os.Exit(1)
	}
	if e = scr.Init(); e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%v\n", e)
		os.Exit(1)
	}
	return scr
}

// redraw orders all elements on the screen to redraw themselves
func (a *appScreen) redraw() {
	for _, obj := range a.objects {
		obj.draw()
	}
}
