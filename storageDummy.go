package main

// implements an in-memory storage engine for quick tests

import "fmt"

type dummyStorage map[string]string

func (s dummyStorage) readEntity(e string) (string, error) {
	d, ok := s[e]
	if !ok {
		return d, fmt.Errorf("entity %v not found", e)
	}
	return d, nil
}

func (s dummyStorage) writeEntity(e string, desc string) error {
	s[e] = desc
	return nil
}

func (s dummyStorage) getAllEntityIDs() []string {
	var keys []string
	for k := range s {
		keys = append(keys, k)
	}
	return keys
}

func (s dummyStorage) newEntityID(e string) {
	if _, ok := s[e]; !ok {
		s[e] = ""
	}
}

func (s dummyStorage) deleteEntity(e string) error {
	delete(s, e)
	return nil
}

func (s dummyStorage) initDemoData() dummyStorage {
	var d = dummyStorage{
		"facebook": "lots of blue",
		"amazon":   "virtual machines everywhere",
		"google":   "gBus",
		"jabra":    "talk time",
		"citrix":   "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu rutrum dui, id suscipit leo. Ut posuere sodales malesuada. Ut vel felis eu diam venenatis placerat. Fusce in eleifend nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget nunc nec justo tincidunt laoreet. Mauris ut orci eget eros lacinia accumsan vel non arcu.",
	}
	return d
}
