package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

type sqliteStorage struct {
	DB *sql.DB
}

func (s sqliteStorage) openUsingFile(file string) *sqliteStorage {
	db, err := sql.Open("sqlite3", "./"+file)
	if err != nil {
		log.Fatal(err)
	}

	sqlStmt := `
	create table if not exists data (employer TEXT NOT NULL PRIMARY KEY, notes TEXT DEFAULT "");
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		panic(err)
	}

	return &sqliteStorage{
		DB: db,
	}
}

func (s sqliteStorage) readEntity(e string) (string, error) {
	stmt, err := s.DB.Prepare(`
		SELECT notes FROM data WHERE employer=?;
	`)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	var desc string
	err = stmt.QueryRow(e).Scan(&desc)
	if err != nil {
		log.Panic(err)
	}
	return desc, nil
}

func (s sqliteStorage) writeEntity(e string, desc string) error {
	stmt, err := s.DB.Prepare(`
		UPDATE data SET notes=? WHERE employer=?;
	`)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(desc, e)
	if err != nil {
		log.Panic(err)
	}
	return nil
}

func (s sqliteStorage) getAllEntityIDs() []string {
	stmt, err := s.DB.Prepare(`
		SELECT employer FROM data;
	`)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	var resp []string
	rows, err := stmt.Query()
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			log.Fatal(err)
		}
		resp = append(resp, name)
	}
	return resp
}
func (s sqliteStorage) newEntityID(e string) {
	var stmt, err = s.DB.Prepare(`
		INSERT INTO data(employer) VALUES(?);
	`)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	_, _ = stmt.Exec(e)
	return
}

func (s sqliteStorage) deleteEntity(e string) error {
	stmt, err := s.DB.Prepare(`
		DELETE FROM data WHERE employer = ?;
	`)
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(e)
	if err != nil {
		log.Panic(err)
	}
	return nil
}
