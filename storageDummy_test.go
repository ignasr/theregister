package main

import "testing"

func TestWriteEntity(t *testing.T) {
	var ds = make(dummyStorage)
	var testDesc = "first"

	ds.writeEntity("1", testDesc)
	if ds["1"] != testDesc {
		t.Errorf("Wanted %v got %v.", testDesc, ds["1"])
	}
}
