package main

type GUITextBox struct {
	left, right, top, bottom     int // widget boundaries
	leftT, rightT, topT, bottomT int // text boundaries
	text                         string
	db                           storage
	parent                       *appScreen
}

// clear clears the text
func (g *GUITextBox) clear() {
	g.setText("")
	g.update()
}

// update must be called on every outside event
func (g *GUITextBox) update() {
	g.draw()
}

// draws the element to the screen buffer
func (g *GUITextBox) draw() {
	drawGuides(g.left, g.right, g.top, g.bottom, g.parent)
	lines := textToLines(g.text, g.rightT-g.leftT+1)
	for idx := 0; idx <= g.bottomT-g.topT; idx++ {
		fillLine(getString(lines, idx), g.leftT, g.rightT, g.topT+idx, g.parent)
	}
	g.parent.Show()
}

// setText sets the text box text
func (g *GUITextBox) setText(t string) {
	g.text = t
	g.update()
}

// ping is triggered by parents on their changes
func (g *GUITextBox) ping(s string) {
	// TODO find data about subject s and set the text to data
	defer g.draw()
	if s == "" {
		g.setText("")
		return
	}
	e, err := g.db.readEntity(s)
	if err != nil {
		g.setText("error reading the value")
	} else {
		g.setText(e)
	}
}

// newTextBox returns a new TextBox struct
func newTextBox(left, right, top, bottom int, text string, db storage, parent *appScreen) GUITextBox {
	return GUITextBox{
		left:    left,
		right:   right,
		top:     top,
		bottom:  bottom,
		leftT:   left + 1,
		rightT:  right - 1,
		topT:    top + 1,
		bottomT: bottom - 1,
		text:    text,
		db:      db,
		parent:  parent,
	}
}

// resize called on screen resize
func (g *GUITextBox) resize(w, h, bottom int) {
	g.right = w - 1
	g.rightT = w - 2
	g.bottom = h - 5
	g.bottomT = h - 6
	g.draw()
}
