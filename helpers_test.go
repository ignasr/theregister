package main

import "testing"

func TestStringSliceCmp(t *testing.T) {
	var tests = []struct {
		a    []string
		b    []string
		want bool
	}{
		{[]string{}, []string{}, true},
		{[]string{""}, []string{""}, true},
		{[]string{"a"}, []string{"a"}, true},
		{[]string{"aaaa"}, []string{"aaaa"}, true},
		{[]string{"a", "b"}, []string{"a", "b"}, true},
		{[]string{"a"}, []string{"b"}, false},
		{[]string{"aaaa"}, []string{"aaa"}, false},
		{[]string{"a", "b"}, []string{"a", "b", "c"}, false},
		{[]string{"a", "b"}, []string{"a", "c"}, false},
	}

	for _, tc := range tests {
		if stringSliceCmp(tc.a, tc.b) != tc.want {
			t.Errorf("Failed for: %v and %v", tc.a, tc.b)
		}
	}
}

func TestTextToLines(t *testing.T) {
	var tests = []struct {
		s     string
		width int
		want  []string
	}{
		{"", 9, []string{""}},
		{".", 9, []string{"."}},
		{"aa", 1, []string{"a", "a"}},
		{"12345", 3, []string{"123", "45"}},
		{"1234\n5", 3, []string{"123", "4", "5"}},
		{"1äüß\n5", 2, []string{"1ä", "üß", "5"}},
		{"1äüß\n5", 3, []string{"1äü", "ß", "5"}},
		{"1äüß\n5\n", 3, []string{"1äü", "ß", "5", ""}},
		{"1äüß\n5\n\nabc\n", 3, []string{"1äü", "ß", "5", "", "abc", ""}},
	}

	for _, tc := range tests {
		got := textToLines(tc.s, tc.width)
		if !stringSliceCmp(got, tc.want) {
			t.Errorf("Wanted %v got %v.", tc.want, got)
		}
	}
}

func TestSliceString(t *testing.T) {
	var tests = []struct {
		s     string
		width int
		want  []string
	}{
		{"", 3, []string{""}},
		{".", 9, []string{"."}},
		{"aa", 1, []string{"a", "a"}},
		{"12345", 3, []string{"123", "45"}},
		{"aä", 3, []string{"aä"}},
		{"aä", 1, []string{"a", "ä"}},
		{"aäüßäü", 2, []string{"aä", "üß", "äü"}},
		{"aäüßäü", 3, []string{"aäü", "ßäü"}},
	}

	for _, tc := range tests {
		got := sliceString(tc.s, tc.width)
		if !stringSliceCmp(got, tc.want) {
			t.Errorf("Wanted %v got %v.", tc.want, got)
		}
	}
}
