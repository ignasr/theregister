package main

import (
	"sort"
)

type GUIListBox struct {
	left, right, top, bottom     int // widget boundaries
	leftT, rightT, topT, bottomT int // text boundaries
	heightT                      int // text height
	entries                      []string
	idx                          int // show the selected entry. -1 when []entries is empty
	parent                       *appScreen
	children                     []*Child
	db                           storage
}

// newListBox returns a new ListBox struct
func newListBox(left, right, top, bottom int, db storage, parent *appScreen) GUIListBox {
	list := db.getAllEntityIDs()
	sort.Strings(list)
	return GUIListBox{
		entries: list,
		idx:     -1,
		left:    left,
		right:   right,
		top:     top,
		bottom:  bottom,
		leftT:   left + 1,
		rightT:  right - 1,
		topT:    top + 1,
		bottomT: bottom - 1,
		heightT: bottom - top - 2,
		parent:  parent,
		db:      db,
	}
}

// updateCursor ensures that selected element is valid
func (g *GUIListBox) updateCursor() {
	if len(g.entries) == 0 {
		// if list box is empty
		g.idx = -1
	} else if g.idx >= len(g.entries) {
		// if off-bounds
		g.idx = len(g.entries) - 1
	} else if pos := findStringPos(g.entries, g.selectedText()); pos == -1 {
		// if selection invalid, select 0th entry
		g.idx = 0
	}
	g.update()
}

// selectEntity selects the entry s in the list box
func (g *GUIListBox) selectEntity(s string) {
	g.refreshEntities()
	for i, e := range g.entries {
		if e == s {
			g.idx = i
		}
	}
	g.ping(s)
}

// moveDown is triggered when user presses the up arrow
func (g *GUIListBox) moveUp() {
	if g.idx > 0 {
		g.idx--
	}
	g.update()
}

// moveDown is triggered when user presses the down arrow
func (g *GUIListBox) moveDown() {
	if g.idx < len(g.entries)-1 {
		g.idx++
	}
	g.update()
}

// setEntries sets slice l as the entry list in the list box
func (g *GUIListBox) setEntries(l []string) {
	g.entries = l
}

// refreshEntities checks the entity list for new entities
func (g *GUIListBox) refreshEntities() {
	g.setEntries(g.getAllEntries())
}

// edit edits the currently selected entry
func (g *GUIListBox) edit(db storage, editor string) {
	if g.idx != -1 {
		_ = editEntity(g.selectedText(), db, editor)
	}
}

func (g *GUIListBox) delete(db storage) {
	if g.idx != -1 {
		_ = db.deleteEntity(g.selectedText())
	}
}

// update must be called on every widget update
func (g *GUIListBox) update() {
	g.draw()
	g.notifyChildren()
}

// ping triggered by parents on their changes
func (g *GUIListBox) ping(s string) {
	g.setEntries(searchFor(s, g.getAllEntries()))
	g.updateCursor()
	g.draw()
}

// getAllEntries returns all known entities
func (g *GUIListBox) getAllEntries() []string {
	ids := g.db.getAllEntityIDs()
	sort.Strings(ids)
	return ids
}

// draw draws the element on the screen
func (g *GUIListBox) draw() {
	drawGuides(g.left, g.right, g.top, g.bottom, g.parent)
	shift := 0 // used if the entry list is bigger than height of the list box
	if g.idx >= g.heightT-1 {
		// preview next entry
		shift = g.idx - g.heightT + 1
		// exception for last entry
		if g.idx == len(g.entries)-1 {
			shift--
		}
	}
	for boxLine := 0; boxLine <= g.heightT; boxLine++ {
		entryRepr := getString(g.entries, boxLine+shift)
		if boxLine+shift == g.idx {
			entryRepr += " *"
		}
		fillLine(entryRepr, g.leftT, g.rightT, g.topT+boxLine, g.parent)
	}
	g.parent.Show()
}

// registerChildStatusBox registers a statusbox as a child object
func (g *GUIListBox) registerChildStatusBox(child *GUIStatusBox) {
	g.children = append(g.children, &Child{
		GUIChild: child,
	})
}

// registerChildTextBox registers a textbox as a child object
func (g *GUIListBox) registerChildTextBox(child *GUITextBox) {
	g.children = append(g.children, &Child{
		GUIChild: child,
	})
}

// notifyChildren notifies all children
func (g *GUIListBox) notifyChildren() {
	for _, c := range g.children {
		c.GUIChild.ping(g.selectedText())
	}
}

// selectedText returns string selected in the list box
func (g *GUIListBox) selectedText() string {
	if g.idx >= 0 {
		return g.entries[g.idx]
	} else {
		return ""
	}
}

// isEmpty checks if the list box is empty
func (g *GUIListBox) isEmpty() bool {
	if g.idx == -1 {
		return true
	}
	return false
}

// resize called on screen resize
func (g *GUIListBox) resize(w, h, bottom int) {
	g.bottom = bottom
	g.bottomT = bottom - 1
	g.heightT = g.bottom - g.top - 2
	g.draw()
}
