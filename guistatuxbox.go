package main

type GUIStatusBox struct {
	left, right, top, bottom     int
	leftT, rightT, topT, bottomT int
	text                         string
	parent                       *appScreen
}

// newFilterBox returns a new FilterBox struct
func newStatusBox(left, right, top int, text string, parent *appScreen) GUIStatusBox {
	return GUIStatusBox{
		left:    left,
		right:   right,
		top:     top,
		bottom:  top + 2,
		leftT:   left + 1,
		rightT:  right - 1,
		topT:    top + 1,
		bottomT: top + 1,
		text:    text,
		parent:  parent,
	}
}

// setText sets the status box statusText
func (g *GUIStatusBox) setText(t string) {
	g.text = t
	g.draw()
}

// draw refreshes the GUIStatusBox on the screen
func (g *GUIStatusBox) draw() {
	drawGuides(g.left, g.right, g.top, g.bottom, g.parent)
	fillLine(g.text, g.leftT, g.rightT-g.leftT, g.topT, g.parent)
	g.parent.Show()
}

// ping triggered by parents on their changes
func (g *GUIStatusBox) ping(s string) {
	g.setText(s)
	g.draw()
}

// resize called on screen resize
func (g *GUIStatusBox) resize(w, h int) {
	g.right = w - 1
	g.rightT = w - 2
	g.top = h - 3
	g.topT = h - 2
	g.bottom = h - 1
	g.bottomT = h - 2
	g.draw()
}
