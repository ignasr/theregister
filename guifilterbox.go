package main

type GUIFilterBox struct {
	left, right, top, bottom int
	text                     string
	parent                   *appScreen
	children                 []*Child
}

// newFilterBox returns a new FilterBox struct
func newFilterBox(left, right, top, bottom int, text string, parent *appScreen) GUIFilterBox {
	return GUIFilterBox{
		left:   left,
		right:  right,
		top:    top,
		bottom: bottom,
		text:   text,
		parent: parent,
	}
}

// adds a string to the filter box view
func (g *GUIFilterBox) add(c string) {
	g.text = g.text + c
	g.update()
}

// clear clears the search field
func (g *GUIFilterBox) clear() {
	fillLine("", g.left, g.right, g.top, g.parent)
	g.text = ""
	g.update()
}

// update must be called on every widget update
func (g *GUIFilterBox) update() {
	g.draw()
	g.notifyChildren()
}

// draw draws the element on the screen
func (g *GUIFilterBox) draw() {
	drawGuides(g.left, g.right, g.top, g.bottom, g.parent)
	fillLine(g.text, g.left+1, g.right-1, g.top+1, g.parent)
	g.parent.Show()
}

// registerChildListBox registers a list box as a child object
func (g *GUIFilterBox) registerChildListBox(child *GUIListBox) {
	g.children = append(g.children, &Child{
		GUIChild: child,
	})
}

// ping triggered by parents on their changes
func (g *GUIFilterBox) ping(_ string) {
	return
}

// notifyChildren notifies all children
func (g *GUIFilterBox) notifyChildren() {
	for _, c := range g.children {
		c.GUIChild.ping(g.text)
	}
}

// resize called on screen resize
func (g *GUIFilterBox) resize(w, h int) {
	g.right = w - 1
	g.draw()
}
