package main

import (
	"github.com/gdamore/tcell"
	"strings"
)

// searchFor return a []slice with values from oldList that have all the runes from str
func searchFor(str string, oldList []string) []string {
	var newList []string
	for _, candidate := range oldList {
		if containsAllChars(candidate, str) {
			newList = append(newList, candidate)
		}
	}
	return newList
}

// containsAllChars checks if s contains all runes from substr
func containsAllChars(s, substr string) bool {
	for _, c := range substr {
		if !strings.Contains(s, string(c)) {
			return false
		}
	}
	return true
}

// stringSliceCmp compares two slices
func stringSliceCmp(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// textToLines splits string s to slices of length w
// s can include newlines
func textToLines(s string, w int) []string {
	if len(s) == 0 {
		return []string{""}
	}

	noNewlines := strings.Split(s, "\n")
	var ret []string
	for _, j := range noNewlines {
		if j == "" {
			ret = append(ret, j)
		} else {
			slicedLine := sliceString(j, w)
			for _, line := range slicedLine {
				ret = append(ret, line)
			}
		}
	}
	return ret
}

// findStringPos finds the string position in a []string. Returns -1 if not found
func findStringPos(sl []string, s string) int {
	for i, v := range sl {
		if v == s {
			return i
		}
	}
	return -1
}

// fillLine prints a line using s, then fills the space to the right with spaces
func fillLine(s string, left, right, y int, scr *appScreen) {
	r := []rune(s)
	for idx := 0; idx < right-left+1; idx++ {
		scr.SetContent(left+idx, y, getRune(r, idx), nil, tcell.StyleDefault)
	}
}

// clearLine removes a line of character from the screen
func clearLine(left, right, y int, scr *appScreen) {
	fillLine("", left, right, y, scr)
}

// fillColumn prints a column using s, then fills the space down with spaces
func fillColumn(s string, top, bottom, x int, scr *appScreen) {
	l := len(s)
	i := 0
	for ; i <= top-bottom; i++ {
		if i < l {
			scr.SetContent(x, top+i, rune(s[i]), nil, tcell.StyleDefault)
		} else {
			scr.SetContent(x, top+i, ' ', nil, tcell.StyleDefault)
		}
	}
}

// clearColumn removes a column of character from the screen
func clearColumn(top, bottom, x int, scr *appScreen) {
	fillColumn("", top, bottom, x, scr)
}

// getString returns the idx element of s or an empty string
func getString(s []string, idx int) string {
	if len(s) != 0 && idx >= 0 && idx < len(s) {
		return s[idx]
	}
	return ""
}

// getRune returns the idx rune of r or an empty rune
func getRune(r []rune, idx int) rune {
	if len(r) != 0 && idx >= 0 && idx < len(r) {
		return r[idx]
	}
	return ' '
}

func drawGuides(left, right, top, bottom int, scr *appScreen) {
	// RuneULCorner = '┌'
	// RuneURCorner = '┐'
	// RuneLLCorner = '└'
	// RuneLRCorner = '┘'
	scr.SetContent(left, top, tcell.RuneULCorner, nil, tcell.StyleDefault)
	scr.SetContent(right, top, tcell.RuneURCorner, nil, tcell.StyleDefault)
	scr.SetContent(left, bottom, tcell.RuneLLCorner, nil, tcell.StyleDefault)
	scr.SetContent(right, bottom, tcell.RuneLRCorner, nil, tcell.StyleDefault)
	clearLine(left+1, right-1, top, scr)
	clearLine(left+1, right-1, bottom, scr)
	clearColumn(top+1, bottom-1, left, scr)
	clearColumn(top+1, bottom-1, right, scr)
}

// sliceString returns a slice of strings, where each element has at max w characters
// s must not contain newlines
func sliceString(s string, w int) []string {
	if s == "" {
		return []string{""}
	}

	var ret []string
	r := []rune(s)
	x := 0
	for ; x < len(r); x += w {
		if x+w < len(r) {
			ret = append(ret, string(r[x:x+w]))
		} else {
			ret = append(ret, string(r[x:]))
		}
	}
	return ret
}
