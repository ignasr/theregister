package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

func editEntity(e string, ds storage, editor string) error {
	description, _ := ds.readEntity(e)
	description = liveEditString(description, editor)
	if *debug {
		fmt.Printf("DEBUG: Writting to entity %v: %v", e, description)
	}
	err := ds.writeEntity(e, description)
	if err != nil {
		return err
	}
	return nil
}

// liveEditString edits the string s with an editor and returns a modified string.
func liveEditString(s string, editor string) string {
	tmpDir := os.Getenv("TMPDIR")

	randSuffix := randString(5)
	tmpFileName := tmpDir + appName + ".tmp." + randSuffix
	if *debug {
		fmt.Printf("DEBUG: Using tmp file %v.\n", tmpFileName)
	}
	writeStringToTMP(tmpFileName, s)

	args := []string{tmpFileName}
	cmd := exec.Command(editor, args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	// TODO save the contents to storage and remove the file
	newDesc, err := readStringFromTMP(tmpFileName)
	if err != nil {
		fmt.Printf("Error reading from tmp file %v.", tmpFileName)
		os.Exit(1)
	}
	os.Remove(tmpFileName)
	return newDesc
}

// writeStringToTMP writes string s to a tmp file f
func writeStringToTMP(f, s string) error {
	data := []byte(s)
	err := ioutil.WriteFile(f, data, 0644)
	return err
}

// readStringFromTMP reads and returns data from file f
func readStringFromTMP(f string) (string, error) {
	data, err := ioutil.ReadFile(f)
	if *debug {
		fmt.Printf("DEBUG: Read from tmp file: %v", string(data))
	}
	return string(data), err
}
