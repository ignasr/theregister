package main

type GUIChild interface {
	ping(string) // called on upstream change
	draw()
}

type Child struct {
	GUIChild
}

type storage interface {
	readEntity(e string) (string, error)     // reads employer description
	writeEntity(e string, desc string) error // writes employer description
	getAllEntityIDs() []string               // gets all employer names
	newEntityID(e string)                    // adds new employer name
	deleteEntity(e string) error             // deletes employer
}
