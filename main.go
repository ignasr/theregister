package main

import (
	"flag"
	"github.com/gdamore/tcell"
)

const appName = "theregister"

var debug = flag.Bool("d", false, "enable debug mode")
var storType = flag.String("t", "sqlite", "which storage engine to use. You can choose from: sqlite, memory.")
var DBFile = flag.String("f", "theregister.db", "file to use as storage for sqlite engine.")
var fileEditor = flag.String("e", "vim", "which editor to use when editing text.")

func main() {
	flag.Parse()

	var st storage
	switch *storType {
	case "memory":
		st = new(dummyStorage).initDemoData()
	default:
		st = new(sqliteStorage).openUsingFile(*DBFile)
	}

	mainCycle(st, *fileEditor)
}

func mainCycle(db storage, fileEditor string) {
	myScreen := appScreen{
		nil,
		initScreen(),
	}
	defer myScreen.Screen.Fini()

	screenWidth, screenHeight := myScreen.Size()
	myScreen.Screen.Clear()

	myFilterBox := newFilterBox(0, screenWidth-1, 0, 2, "", &myScreen)
	myScreen.objects = append(myScreen.objects, &myFilterBox)

	myStatusBox := newStatusBox(0, screenWidth-1, screenHeight-3, "", &myScreen)
	myScreen.objects = append(myScreen.objects, &myStatusBox)

	myListBox := newListBox(0, 20, myFilterBox.bottom+1, myStatusBox.top-1, db, &myScreen)
	myScreen.objects = append(myScreen.objects, &myListBox)
	myListBox.draw()

	myTextBox := newTextBox(myListBox.right+1, screenWidth-1, myFilterBox.bottom+1, myStatusBox.top-1, "", db, &myScreen)
	myScreen.objects = append(myScreen.objects, &myTextBox)

	myFilterBox.registerChildListBox(&myListBox)
	myListBox.registerChildStatusBox(&myStatusBox)
	myListBox.registerChildTextBox(&myTextBox)

	myFilterBox.clear()

	// setting this to true will execute a text editor
	var editExecute = false

	for {
		if editExecute {
			myScreen.Screen.Fini()
			myListBox.edit(db, fileEditor)
			myScreen.Screen = initScreen()
			// reload now data to the textbox
			myTextBox.ping(myListBox.selectedText())
			myScreen.redraw()
			myScreen.Screen.Sync()
			editExecute = false
		}
		ev := myScreen.Screen.PollEvent()
		switch ev := ev.(type) {
		case *tcell.EventKey:
			switch ev.Key() {
			case tcell.KeyRune:
				myFilterBox.add(string(ev.Rune()))
			case tcell.KeyUp:
				myListBox.moveUp()
			case tcell.KeyDown:
				myListBox.moveDown()
			case tcell.KeyBackspace, tcell.KeyBackspace2:
				myFilterBox.clear()
			case tcell.KeyEscape:
				return
			case tcell.KeyCtrlL:
				myScreen.Screen.Sync()
			//default:
			case tcell.KeyF2:
				if !myListBox.isEmpty() {
					editExecute = true
				}
				continue
			case tcell.KeyF3:
				if len(myFilterBox.text) > 0 {
					db.newEntityID(myFilterBox.text)
					myListBox.selectEntity(myFilterBox.text)
				}
			case tcell.KeyF8:
				myListBox.delete(db)
				myListBox.ping(myFilterBox.text)
			}
		case *tcell.EventResize:
			//screenWidth, screenHeight = myScreen.Size()
			//myFilterBox.resize(screenWidth, screenHeight)
			//myStatusBox.resize(screenWidth, screenHeight)
			//myListBox.resize(screenWidth, screenHeight, myStatusBox.top-1)
			//myTextBox.resize(screenWidth, screenHeight, myStatusBox.top-1)
			myScreen.Screen.Sync()
		}
	}

}
