# the register

The Register is a simple terminal app, which you can use to track your job application to various employers with text notes. Data is saved in a SQLite database.

This is my 'learning Golang' project.

## usage

```shell script
go run . -t memory # load some test data 
go run .           # save data to theregister.db
go run . -h        # help
```

- type to search
- up/down arrows to move in the list
- type and press F3 to add an employer
- F2 edits the selected employers description
- F8 removes the selected employer
- Esc quits

## screenshots

![Running with test data.](images/1.png)
![Searching.](images/2.png)

## todo

- simplify registerChild with interfaces
- add employer rename
- remove tmp edit file after editing
- add status for employers (prio, waiting, dropped, ...)
- sqlite should return errors instead of panicking
- widgets auto-resize based on screen size
- command help bar (F2, F3, ...)
- status bar for messages
- use fuzzy search
- check errors everywhere
- ensure that there are no syntax warnings
- increase code coverage
- pack as docker
- gif of usage
- postgres support
